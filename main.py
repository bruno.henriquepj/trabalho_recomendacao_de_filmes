#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import pandas as pd
from scipy.spatial import distance


itens = pd.read_csv(
    'ml-100k/u.item',
    delimiter='|',
    header=None,
    names = [
        'movie id',
        'movie title',
        'release date',
        'video release date',
        'IMDb URL',
        'unknown',
        'Action',
        'Adventure',
        'Animation',
        'Children\'s',
        'Comedy',
        'Crime',
        'Documentary',
        'Drama',
        'Fantasy',
        'Film-Noir',
        'Horror',
        'Musical',
        'Mystery',
        'Romance',
        'Sci-Fi',
        'Thriller',
        'War',
        'Western'
    ]
)


avaliacoes = pd.read_csv(
    'ml-100k/u.data',
    delim_whitespace=True,
    header=None,
    names = [
        'user id',
        'item id',
        'rating',
        'timestam'
    ]
)

# avaliacoes_count = avaliacoes.groupby(['item id'])['rating'].agg(['count'])
# avaliacoes_count.reset_index(drop=True)
# avaliacoes_max = avaliacoes_count[['count']].max(axis=0)
# popularidade =  avaliacoes_count['count'] / int(avaliacoes_max)
# popularidade_df = pd.DataFrame(popularidade)

def get_filmes_proximos(
    filmes_distancia_data_frame,
    titulo_filme,
    quantidade_proximos = 5
):
    id = itens[itens['movie title'] == titulo_filme]['movie id'].values[0]
    filmes = filmes_distancia_data_frame[filmes_distancia_data_frame['filme_um_id'] == id]

    filmes = filmes.sort_values(by='distancia')
    filmes = filmes[0:quantidade_proximos]
    filmes_dict = get_filmes_dict(filmes)
    return filmes_dict


def get_filmes_dict(filmes_distancia_data_frame):
    filmes = []
    for index, row in filmes_distancia_data_frame.iterrows():
        id_dois = int(row['filme_dois_id'])
        filme_dois = itens[itens['movie id'] == id_dois]
        distancia = row['distancia']
        filmes.append({ 'filme': filme_dois.values, 'distancia': distancia })
    return filmes


def calcula_distancias_euclidiana(filmes_df, filme):
    filmes_distancia = []
    for filme_dois in filmes_df.values:
        distancia = distance.euclidean(filme.values[0][5:24], filme_dois[1])
        filmes_distancia.append([
            filme.values[0][0],
            filme_dois[0],
            distancia
        ])
    filmes_distancia_df = pd.DataFrame(
        filmes_distancia,
        columns=[ 'filme_um_id', 'filme_dois_id', 'distancia' ]
    )
    return filmes_distancia_df


def get_filme_por_nome(filmes_df, nome_filme):
    return itens[itens['movie title'] == nome_filme]


def busca_filme_proximos(filmes_df, nome_filme):
    filme = get_filme_por_nome(filmes_df, nome_filme)
    filmes_distancia_df = calcula_distancias_euclidiana(filmes_df, filme)
    filmes_proximos = get_filmes_proximos(filmes_distancia_df, nome_filme)
    return filmes_proximos


# =================================================================
# =================================================================


# Cria DataFrame de id e generos de filmes
filmes = []
for value in itens.values:
    filmes.append([value[0], value[5:24]])
filmes_df = pd.DataFrame(filmes, columns=['id', 'generos'])


import pprint
pp = pprint.PrettyPrinter(indent=4)
filmes_proximos = busca_filme_proximos(filmes_df, 'Star Wars (1977)')
pp.pprint(filmes_proximos)
